module.exports = async app => {
    app.helpers.hbs.registerHelper('momentcalendar', function(date, options) {
        return date.calendar()
    })

    return {
        context: {
            metaTitle: `Collectif de développeurs pour le logiciel libre`,
            metaImage: '/img/md-meta.jpg',
            metaDescription: `Création d'outils Web libres, sites Web personnalisés, et plateformes Web à Montpellier. Nous prêchons sur les coopératives de plateformes et nous nous engageons à travailler principalement avec les coopératives et les associations.`,
            metaName: `Montpedigital - Collectif de développeurs pour le logiciel libre.`,
            metaKeywords: 'platform-co-ops, free software, logiciel libre, montpellier, Hérault, 3400, sites web, web development'
        },
        env: {
            defaultLanguage: 'fr',
            domainn: 'montpedigital.misitioba.com'
        },
        plugins: {
            cleanDistFolders: {
                enabled: process.env.NODE_ENV === 'production',
                preserveFolders: ['img']
            }
        }
    }
}