module.exports = async app => {
    return {
        layout: 'page',
        head_title: 'MONTPEDIGITAL',
        target: '/',
        title: 'I18N_MAIN_PAGE',
        headline: true,
        services: [{
                title: 'Sites Web professionnels',
                image: 'https://images.unsplash.com/photo-1519222970733-f546218fa6d7?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=480&q=80',
                content: ``
            },
            {
                title: 'Plateformes web',
                image: 'https://images.unsplash.com/photo-1457305237443-44c3d5a30b89?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=480&q=80',
                content: ``
            },
            {
                title: 'Soutien technique',
                image: 'https://images.unsplash.com/photo-1454165804606-c3d57bc86b40?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=480&q=80',
                content: ``
            },
            {
                title: 'Formation de développeur Web',
                image: 'https://images.unsplash.com/photo-1555436169-20e93ea9a7ff?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=480&q=80',
                content: ``
            }
        ]
    }
}