module.exports = async app => {
    return {
        layout: 'blog',
        head_title: 'Montpel’libre, l’asso libre tous azimuts',
        title: `Montpel’libre, l’asso libre tous azimuts`,
        metaKeywords: `cloud ethique free software libre logiciel association`,
        article: {
            author: '@javimosch',
            created: app.helpers.moment('27-09-2019 19:50', 'DD-MM-YYYY HH:mm')
        },
        target: `/montpelibre-la-asso-libre-tous-azimuts`
    }
}