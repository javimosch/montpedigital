module.exports = async app => {
    return {
        layout: 'page',
        head_title: 'Contact',
        title: `Contact`,
        target: '/contact',
        hero: {
            title: 'Contact',
            subtitle: 'I18N_CONTACT_SUBTITLE'
        },
        noWrapper: true
    }
}