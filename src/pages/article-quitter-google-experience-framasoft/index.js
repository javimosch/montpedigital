module.exports = async app => {
    return {
        layout: 'blog',
        head_title: `Quitter Google ? L'expérience de l'association Framasoft`,
        title: `Quitter Google ? L'expérience de l'association Framasoft`,
        target: `/quitter-google-la-experience-de-la-asso-framasoft`,
        metaKeywords: `google framasoft association france`,
        article: {
            author: '@javimosch',
            created: app.helpers.moment('25-09-2019 15:30', 'DD-MM-YYYY HH:mm')
        }
    }
}