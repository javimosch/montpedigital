module.exports = async app => {
    return {
        layout: 'blog',
        head_title: 'Existe-t-il un cloud éthique ?',
        title: `Existe-t-il un cloud éthique ?`,
        metaKeywords: `cloud ethique free software libre logiciel`,
        article: {
            author: '@javimosch',
            created: app.helpers.moment('15-09-2019 10:27', 'DD-MM-YYYY HH:mm')
        },
        target: `/existe-t-il-un-cloud-ethique`
    }
}