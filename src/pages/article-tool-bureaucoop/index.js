module.exports = async app => {
    return {
        layout: 'blog',
        head_title: 'Bureaucoop',
        title: `Bureaucoop: Share your desk, never work alone`,
        target: '/platforms/bureaucoop',
        format: 'md',
        article: {
            author: '@javimosch',
            created: app.helpers.moment('05-10-2019 19:40', 'DD-MM-YYYY HH:mm')
        }
    }
}