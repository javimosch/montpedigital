module.exports = async app => {
    return {
        layout: 'page',
        head_title: 'About',
        target: '/about-us',
        hero: {
            title: 'About',
            subtitle: ''
        },
        metaKeywords: `about information developer web javascript javier leandro arancibia`
    }
}