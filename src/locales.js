module.exports = async options => {
    return {
        en: {
            HOME_HEADLINE: `Professional web development at Montpellier`,
            MAIN_PAGE: 'Main Page',
            LEGAL_MENTIONS: 'Legal Mentions',
            SEND: 'Send',
            CONTACT_MESSAGE: 'Available also on WhatsApp',
            SITEMAP: 'Sitemap',
            HQ_ADDRESS: 'HQ Address',
            ABOUT: 'About us',
            CONTACT_SUBTITLE: `Stay in touch`
        },
        fr: {
            HOME_HEADLINE: `Développement web professionnel à Montpellier`,
            MAIN_PAGE: "Accueil",
            LEGAL_MENTIONS: 'Mentions Légales',
            SEND: 'Envoyer',
            CONTACT_MESSAGE: 'Disponible sur WhatsApp',
            SITEMAP: 'Plan du site',
            HQ_ADDRESS: 'Addrese QG',
            ABOUT: 'À propos',
            CONTACT_SUBTITLE: `Nous sommes ici`
        }
    }
}